package org.launchcode.devops.mapnotesapi.NotesController;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.launchcode.devops.mapnotesapi.TestUtils.IntegrationTestConfig;

@IntegrationTestConfig
public class GetNotesTests {

  @Test
  @DisplayName("[empty state] GET /notes: an empty JSON list")
  public void getNotesEmpty() throws Exception {
    assertTrue(false);
  }

  @Test
  @DisplayName("[populated state] GET /notes: a JSON list of Note entities")
  public void getNotesPopulated() throws Exception {
    assertTrue(false);
  }
}
