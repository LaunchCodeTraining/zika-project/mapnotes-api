package org.launchcode.devops.mapnotesapi.TestUtils;

import java.util.ArrayList;
import java.util.List;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;

import org.launchcode.devops.mapnotesapi.models.Feature.NoteFeatureEntity;

public class NoteFeatureEntityUtil {

    public static List<NoteFeatureEntity> getTestNoteFeatureEntities() {
        List<NoteFeatureEntity> noteFeatureEntities = new ArrayList<>();
        Coordinate[] coordinatesArray = {
            new Coordinate(-9438256.383064654,2648494.7591272676),
            new Coordinate(-9040547.558033329,2728036.524133533),
            new Coordinate(-8265015.34922224,2409869.4641084713),
            new Coordinate(-8205359.025467541,2220957.772218591),
            new Coordinate(-8642838.733002001,2151358.727838109),
            new Coordinate(-8961005.793027062,2320384.978476423),
            new Coordinate(-9398485.500561522,2439697.625985821),
            new Coordinate(-9438256.383064654,2648494.7591272676)
        };
        GeometryFactory geometryFactory = new GeometryFactory();
        noteFeatureEntities.add(new NoteFeatureEntity(geometryFactory.createPolygon(coordinatesArray)));
        return noteFeatureEntities;
    }

    //{"type":"FeatureCollection","features":[{"type":"Feature","geometry":{"type":"Polygon","coordinates":[[[-9438256.383064654,2648494.7591272676],[-9040547.558033329,2728036.524133533],[-8265015.34922224,2409869.4641084713],[-8205359.025467541,2220957.772218591],[-8642838.733002001,2151358.727838109],[-8961005.793027062,2320384.978476423],[-9398485.500561522,2439697.625985821],[-9438256.383064654,2648494.7591272676]]]},"properties":null}]}
    
}
