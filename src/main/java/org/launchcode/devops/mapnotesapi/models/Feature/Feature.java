package org.launchcode.devops.mapnotesapi.models.Feature;

import com.bedatadriven.jackson.datatype.jts.serialization.GeometryDeserializer;
import com.bedatadriven.jackson.datatype.jts.serialization.GeometrySerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.vividsolutions.jts.geom.Geometry;

// you will need to finish this class. What's missing?

public class Feature {
  private long id;
  @JsonSerialize(using = GeometrySerializer.class)
  @JsonDeserialize(using = GeometryDeserializer.class)
  private Geometry geometry;
  private final String type = "Feature";

  public Feature() {}

  public Feature(Geometry geometry) {
    this.geometry = geometry;
  }

  public static Feature fromNoteFeatureEntity(NoteFeatureEntity noteFeatureEntity) {
    // how should a new feature be created here? what will it need from noteFeatureEntity?
    return new Feature();
  }

  public NoteFeatureEntity toNoteFeatureEntity() {
    // how should a new NoteFeatureEntity be created here? what will it need from this instantiated Feature object?
    return new NoteFeatureEntity();
  }
}
