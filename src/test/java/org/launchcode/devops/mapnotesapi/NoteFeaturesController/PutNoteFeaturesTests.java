package org.launchcode.devops.mapnotesapi.NoteFeaturesController;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.launchcode.devops.mapnotesapi.TestUtils.IntegrationTestConfig;
import org.launchcode.devops.mapnotesapi.TestUtils.NoteFeatureEntityUtil;
import org.launchcode.devops.mapnotesapi.models.Feature.NoteFeatureEntity;
@IntegrationTestConfig
public class PutNoteFeaturesTests {

    @Test
    @DisplayName("PUT /notes/{noteId}/features: 404")
    public void putNonNoteFeatures() throws Exception {
        // creates a pre-populated list of valid NoteFeatureEntities for testing purposes
        List<NoteFeatureEntity> featureEntities = NoteFeatureEntityUtil.getTestNoteFeatureEntities();
        
        assertTrue(false);
    }

    @Test
    @DisplayName("PUT /notes/{noteId}/features: 200")
    public void putNoteFeatures() throws Exception {
        // creates a pre-populated list of valid NoteFeatureEntities for testing purposes
        List<NoteFeatureEntity> noteFeatureEntities = NoteFeatureEntityUtil.getTestNoteFeatureEntities();

        assertTrue(false);
    }

}
