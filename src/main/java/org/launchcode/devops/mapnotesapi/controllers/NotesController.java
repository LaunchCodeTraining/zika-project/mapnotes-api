package org.launchcode.devops.mapnotesapi.controllers;

import org.launchcode.devops.mapnotesapi.models.Note.OutboundNoteRepresentation;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

// you will have to complete this class

// what path is this controller responsible for?
// what are each of the endpoints responsible for?

public class NotesController {

  public ResponseEntity<Object> getNotes() {
    return ResponseEntity.status(418).build();
  }

  public ResponseEntity<Object> createNote() {

    return ResponseEntity.status(418).build();
  }

  public ResponseEntity<OutboundNoteRepresentation> getNote() {
    
    return ResponseEntity.status(418).build();
  }

  public ResponseEntity<Object> deleteNote() {
    
    return ResponseEntity.status(418).build();
  }
}