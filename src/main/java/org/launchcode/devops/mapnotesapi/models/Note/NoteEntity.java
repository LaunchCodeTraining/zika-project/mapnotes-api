package org.launchcode.devops.mapnotesapi.models.Note;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;

// you will need to complete this entity.

// outside of the provided body property what else does a Note Entity need?

@Table(name = "notes")
public class NoteEntity {

  // https://www.baeldung.com/jpa-annotation-postgresql-text-type#column-annotation
  @Lob
  private String body;

  public NoteEntity() {
    
  }
}
