package org.launchcode.devops.mapnotesapi.NotesController;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.launchcode.devops.mapnotesapi.TestUtils.IntegrationTestConfig;

@IntegrationTestConfig
public class GetNoteTests {
  
  @Test
  @DisplayName("[empty state] GET /notes/{noteId}: 404 status")
  public void getNoteEmpty() throws Exception {
    assertTrue(false);
  }

  @Test
  @DisplayName("[populated state] GET /notes/{noteId}: the corresponding Note entity")
  public void getNotePopulated() throws Exception {
    assertTrue(false);
  }
}
